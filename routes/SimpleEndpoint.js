const express = require('express');
const bodyParser = require('body-parser');

class SimpleEndpoint {
  constructor(name, items = []) {
    this.endpointName = name;
    this.items = {};

    this.addItem = this.addItem.bind(this);
    this.addItems = this.addItems.bind(this);
    this.handleGet = this.handleGet.bind(this);
    this.handlePost = this.handlePost.bind(this);
    this.handlePut = this.handlePut.bind(this);
    this.handleDelete = this.handleDelete.bind(this);

    this.addItems(items);
    this.createRouter().handleRequests();
  }

  static getValidatedItem(item) {
    if (!item) return null;

    const isValidItem = Boolean(item.id && item.name && item.description);

    return isValidItem ? item : null;
  }

  static handleAll(req, res, next) {
    res.status(200).set('Content-Type', 'application/json');
    next();
  }

  handleGet(req, res) {
    const itemId = req.params.id;

    if (itemId) {
      const item = this.items[itemId];

      if (item) {
        res.json(item);
      } else {
        res.status(404).end();
      }
    } else {
      res.json(Object.values(this.items));
    }
  }

  handlePost(req, res) {
    const itemId = req.params.id;
    const newItem = req.body;

    if (itemId || !newItem) {
      res.status(403).end();
    } else {
      const lastId = Object.keys(this.items).sort().reverse().slice(0, 1)[0] || 0;
      const unusedId = Number(lastId) + 1;
      const newItemWithId = {
        id: unusedId,
        ...newItem,
      };
      const validatedItem = SimpleEndpoint.getValidatedItem(newItemWithId);
      if (validatedItem) {
        this.addItem(validatedItem);
        res.status(201).json(validatedItem);
      } else {
        res.status(422).end();
      }
    }
  }

  handlePut(req, res) {
    const itemId = req.params.id;
    const updatedItem = req.body;

    if (!itemId || !updatedItem) {
      res.status(403).end();
    } else {
      const item = this.items[itemId];
      const isUpdatedItemValid = updatedItem.name || updatedItem.description;

      if (!item) {
        res.status(404).end();
      } else if (!isUpdatedItemValid) {
        res.status(422).end();
      } else {
        item.name = updatedItem.name || item.name;
        item.description = updatedItem.description || item.description;

        res.status(200).json(item);
      }
    }
  }

  handleDelete(req, res) {
    const itemId = req.params.id;

    if (itemId) {
      const item = this.items[itemId];

      if (item) {
        delete this.items[itemId];
        res.end();
      } else {
        res.status(404).end();
      }
    } else {
      this.items = {};
      res.json(Object.values(this.items));
    }
  }

  addItem(item) {
    const validatedItem = SimpleEndpoint.getValidatedItem(item);

    if (validatedItem) {
      this.items[item.id] = {
        type: this.endpointName,
        ...item,
      };
    }
  }

  addItems(items) {
    if (!Array.isArray(items)) return;

    items.forEach(this.addItem);
  }

  createRouter() {
    this.router = express.Router();
    this.router.use(bodyParser.json());

    return this;
  }

  handleRequests() {
    this.router.route('/:id?')
      .all(SimpleEndpoint.handleAll)
      .get(this.handleGet)
      .post(this.handlePost)
      .put(this.handlePut)
      .delete(this.handleDelete);
  }
}

module.exports = SimpleEndpoint;
