const dishes = [
  {
    id: 1,
    name: 'Mexican chicken burger',
    description: 'Ready in under 20 minutes, this burger with spiced chipotle chicken breast, in toasted brioche ' +
      'with guacamole, makes for a satisfying weeknight treat for one'
  },
  {
    id: 2,
    name: 'Turkey, courgetti & feta burgers',
    description: 'Turkey mince, spiralized courgette, feta and mint give these burgers a healthier profile. ' +
      'Serve with a peppery rocket and sweet cherry tomato salad',
  }
];
const promotions = [];
const leaders = [];

exports.dishes = dishes;
exports.promotions = promotions;
exports.leaders = leaders;
