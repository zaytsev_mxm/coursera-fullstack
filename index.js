const express = require('express');
const http = require('http');
const morgan = require('morgan');

const SimpleEndpoint = require('./routes/SimpleEndpoint');
const initialValues = require('./routes/initialValues');

const hostname = 'localhost';
const port = 3000;

const app = express();

app.use(morgan('dev'));

app.use('/dishes', new SimpleEndpoint('dishes', initialValues.dishes).router);
app.use('/promotions', new SimpleEndpoint('promotions', initialValues.promotions).router);
app.use('/leaders', new SimpleEndpoint('leaders', initialValues.leaders).router);

const server = http.createServer(app);

server.listen(port, hostname, () => {
  console.log('Express server is running on port ' + port);
});
